const fs = require(`fs`)

require("dotenv").config({
  path: `.env`,
})

module.exports = {
  siteMetadata: {
    title: `Casey Holzer's Repositories`,
    description: `A filterable, sortable list of Casey Holzer's public repositories on GitHub.`,
    author: `@caseyjhol`,
  },
  plugins: [
    {
      resolve: `gatsby-source-github-api`,
      options: {
          token: process.env.GITHUB_TOKEN,
          graphQLQuery: fs.readFileSync('./github.graphql').toString(),
      },
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Casey Holzer's Repositories`,
        short_name: `Casey Holzer`,
        start_url: `/`,
        background_color: `#FF4630`,
        theme_color: `#FF4630`,
        display: `minimal-ui`
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    {
       resolve: 'gatsby-plugin-offline',
       options: {
          workboxConfig: {
             globPatterns: ['**/*']
          }
       }
    }
  ],
}
