export const parseData = rawData => {
  const data = rawData.data.githubData.data
  const repositories = data.user.repositories.nodes

  repositories.map(value => {
  	value.languages = value.languages.nodes
  	return value
  })

  return { repositories }
}