import React from "react"
import PropTypes from "prop-types"
import Dropdown from "react-bootstrap/Dropdown"
import Icons from "./icons"

const SortMenu = ({ sortBy, results, updateResults }) => {
  const [sort, setSort] = React.useState(sortBy)

  const sortResults = sortBy => {
    const type = sortBy[0]

    if (type === 'name') {
      results.sort((a, b) => (a.name > b.name) ? 1 : -1)
    } else {
      results.sort((a, b) => (a.stargazers.totalCount > b.stargazers.totalCount) ? 1 : -1)
    }
  }

  const handleSelect = eventKey => {
    let newSort = [eventKey]

    // switch sort direction if new sort type is same as previous
    newSort[1] = (newSort[0] === sort[0]) ? (sort[1] === 'asc' ? 'desc' : 'asc') : 'asc'

    setSort(newSort)
    
    if (newSort[0] === sort[0]) {
      results.reverse()
    } else {
      sortResults(newSort)
    }

    updateResults(results);
  }

  const SortIcon = ({active}) => {
    if (!active) return (null);

    return (sort[1] === 'asc' ? <Icons.SortAsc /> : <Icons.SortDesc />)
  }

  const sortTitle = (sort[1] === 'asc' ? 'Ascending' : 'Descending')

  const DropdownItem = ({type}) => (
    <Dropdown.Item eventKey={type} active={sort[0] === type} title={sortTitle} onSelect={handleSelect}>
      {type.charAt(0).toUpperCase() + type.slice(1)} <SortIcon active={sort[0] === type} />
    </Dropdown.Item>
  )

  return (
    <Dropdown className="input-group-append">
      <Dropdown.Toggle variant="outline-secondary" id="dropdown-basic">
        <Icons.Sort /> Sort By
      </Dropdown.Toggle>

      <Dropdown.Menu>
        <DropdownItem type="stargazers" />
        <DropdownItem type="name" />
      </Dropdown.Menu>
    </Dropdown>
  )
}

SortMenu.propTypes = {
  sortBy: PropTypes.array.isRequired,
  results: PropTypes.array.isRequired,
  updateResults: PropTypes.func.isRequired
}

export default SortMenu
