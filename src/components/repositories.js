import React from "react"
import PropTypes from "prop-types"
import Repository from "./repository"

const Repositories = ({ data, layout, searchTerm }) => {
	if (data.length) {
		if (layout === 'grid') {
			return (
				<ul className="card-columns pl-0">
					{ data.map(repoData => (<Repository data={repoData} key={ repoData.id } layout={layout} />)) }
				</ul>
			)
		} else {
			return (
				<div className="card">
					<ul className="list-group list-group-flush">
					{ data.map(repoData => (<Repository data={repoData} key={ repoData.id } layout={layout} />)) }
					</ul>
				</div>
			)
		}
	} else {
		return (
			<div className="card card-body text-center"><strong>No results match "{searchTerm}"</strong></div>
		)
	}
}

Repositories.defaultProps = {
	layout: 'grid'
}

Repositories.propTypes = {
	data: PropTypes.array.isRequired,
	layout: PropTypes.string,
	searchTerm: PropTypes.string
}

export default Repositories
