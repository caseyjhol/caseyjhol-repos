import React from "react"
import PropTypes from "prop-types"
import Icons from "./icons"

const Repository = ({ data, layout }) => {
  const repoInfo = [
    {
      key: 'stars',
      url: data.url + '/stargazers',
      count: data.stargazers.totalCount,
      icon: 'Star'
    },
    {
      key: 'issues',
      url: data.url + '/issues',
      count: data.issues.totalCount,
      icon: 'Issue'
    },
    {
      key: 'forks',
      url: data.url + '/network/members',
      count: data.forks.totalCount,
      icon: 'Fork'
    }
  ]

  return (
    <li className={layout === 'grid' ? 'card card-body' : 'list-group-item'}>
      <h5><a href={ data.url }>{ data.name }</a></h5>
      <p className="text-secondary">{ data.description }</p>
      <ul className="list-inline">
        {
          repoInfo.map(info => { 
            const Icon = Icons[info.icon]

            return (
              <li className="list-inline-item" key={ info.key }>
                <a href={ info.url } className="text-decoration-none text-dark">
                  <Icon className="svg-icon" />
                  { info.count }
                </a>
              </li>
            )
          })
        }
        {
          data.languages.map(language => { 
            return (
              <li className="list-inline-item float-right" key={ language.id }>
                <span className="repo-language-color" style={{background: language.color}}></span>
                <span className="text-secondary small">
                  { language.name }
                </span>
              </li>
            )
          })
        }
      </ul>
    </li>
  )
}

Repository.defaultProps = {
  layout: 'grid'
}

Repository.propTypes = {
  data: PropTypes.object.isRequired,
  layout: PropTypes.string
}

export default Repository
