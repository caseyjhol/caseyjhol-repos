import React from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout"
import Repositories from "../components/repositories"
import SEO from "../components/seo"
import Icons from "../components/icons"
import SortMenu from "../components/sort"
import { parseData } from "../parsers/dataParser"

class IndexPage extends React.Component {
  constructor(props) {
    super(props)

    const { repositories } = parseData(props)

    this.state = {
      searchTerm: '',
      repos: repositories,
      filteredRepos: repositories,
      layout: 'grid'
    }

    this.handleChange = this.handleChange.bind(this)
    this.updateResults = this.updateResults.bind(this)
    this.changeLayout = this.changeLayout.bind(this)
  }

  handleChange = event => {
    const searchTerm = event.target.value

    this.setState((state) => ({
      searchTerm,
      filteredRepos: searchTerm
      ? state.repos.filter(repository =>
        repository.name.toLowerCase().includes(searchTerm.toLowerCase())
      ) 
      : state.repos
    }))
  }

  changeLayout = event => {
    this.setState((state) => ({
      layout: state.layout === 'grid' ? 'list' : 'grid'
    }))
  }

  updateResults = results => {
    this.setState({
      filteredRepos: results
    })
  }

  render () {
    return (
      <Layout>
        <SEO title="Home" />
        <div className="form-group">
          <div className="input-group input-group-lg">
            <input
              type="search"
              className="form-control"
              placeholder="Start typing to filter..."
              value={this.state.searchTerm}
              onChange={this.handleChange}
            />
            <SortMenu sortBy={['stargazers', 'desc']} results={this.state.filteredRepos} updateResults={this.updateResults} />
            <div className="input-group-append">
              <button type="button" className="btn btn-outline-secondary" onClick={this.changeLayout} title={this.state.layout === 'grid' ? 'List View' : 'Grid View'}>
                {this.state.layout === 'grid' ? <Icons.List className="svg-icon" /> : <Icons.Grid className="svg-icon" />}
              </button>
            </div>
          </div>
        </div>
        <Repositories data={this.state.filteredRepos} layout={this.state.layout} searchTerm={this.state.searchTerm} />
      </Layout>
    )
  }
}

export default IndexPage

export const query = graphql`
  query onGithub {
    githubData {
      data {
        user {
          repositories {
            nodes {
              id
              name
              url
              description
              stargazers {
                totalCount
              }
              issues {
                totalCount
              }
              forks {
                totalCount
              }
              languages {
                nodes {
                  color
                  name
                  id
                }
              }
            }
          }
        }
      }
    }
  }
`;